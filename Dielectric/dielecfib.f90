PROGRAM DIELECFIB
IMPLICIT NONE
REAL (KIND=8), DIMENSION (3,3) :: dtens
REAL (KIND=8), DIMENSION (3,3) :: Q
REAL (KIND=8), DIMENSION (3,3) :: drotten, dtexten
REAL (KIND=8) :: phi1, phi, phi2, pi, beta, R
REAL (KIND=8) :: a, b, c, d, dphi, dbeta, omeg, su, total
INTEGER :: i, j, k, l, m, n, nn, npas, np, nphi, nb, nbeta, nt
character(len=50) :: entra

!dV/V = (1/4*pi)*R(phi,beta)*sin(phi)*dphi*dbeta [Bunge, eq(5.13)]
R(phi,beta)= (1.d0/su) * exp(-(180.d0*phi/(pi*omeg))**2) * sin(phi)

nphi = 128*8; nbeta = 256*1

pi = 3.141592653589793D+00

a = 0.d0; b = pi; c = 0.d0; d = 2.d0*pi
dphi= (b-a)/dble(nphi); dbeta=(d-c)/dble(nbeta)

!call system('cls')
write(*,'(/A)')' Dielectric constant for axial textures'
!write(*,'(/A$)')' Enter data file: '
!read (*, '(A)') entra
write(*,'(/A)')' Single crystal tensor in data file:'

!OPEN(unit=1, status='old',file = 'BNBT5_dielectES.dat', action='read')
OPEN(unit=1, status='old',file = 'BNBT5_dielectBT.dat', action='read')
!OPEN(unit=1, status='old',file = 'BaTiO3_diel.dat', action='read')
!OPEN(unit=1, status='old',file = 'Asim_diel.dat', action='read')

!OPEN(unit=1, status='old',file = entra, action='read')

Do m=1,3
    read(1,*) (dtens(m,i), i=1,3)
    write (*,'(3F10.2)')(dtens(m,i), i=1,3)
End Do
CLOSE(1)

write(*,'(/A$)')' Enter Omega (degrees): '
read(*,*) omeg

call inte2D(omeg, su)

phi1=0.d0   !Fibre texture, Bunge convention

! START THE GREAT LOOP FOR POLYCRYSTAL TENSOR COMPONENTS
Do i = 1, 3
   Do j = 1, 3
        dtexten(i,j) =0.d0  ! Setting to "0" the textured polycrystal dielectric tensor component:
           do np =1, nphi  ! loop on polar angle
                phi = a + dphi/2.d0 + dble(np - 1)*dphi
                do nb = 1, nbeta   ! loop on azimuth
                        beta = c + dbeta/2.d0 + dble(nb - 1)*dbeta
                        phi2= (pi/2.d0) - beta    ! Bunge convention
                        Q(1,1) =  cos(phi2)* cos(phi1) - cos(phi)*sin(phi1)*sin(phi2)
                        Q(1,2) =  cos(phi2)* sin(phi1) + cos(phi)*cos(phi1)*sin(phi2)
                        Q(1,3) =  sin(phi2)* sin(phi)
                        Q(2,1) = -sin(phi2)* cos(phi1) - cos(phi)*sin(phi1)*cos(phi2)
                        Q(2,2) = -sin(phi2)* sin(phi1) + cos(phi)*cos(phi1)*cos(phi2)
                        Q(2,3) =  cos(phi2)* sin(phi)
                        Q(3,1) =  sin(phi) * sin(phi1)
                        Q(3,2) = -sin(phi) * cos(phi1)
                        Q(3,3) =  cos(phi)
                        ! Calculating a rotated crystal dielectric tensor component:
                        Do l=1,3
                            Do m = 1, 3
                                  drotten(l,m) =0.d0
                            End Do
                        End Do
                        Do l=1,3
                           Do m = 1, 3
                                 drotten(i,j) = drotten(i,j) + Q(i,l)*Q(j,m)*dtens(l,m)
                           End Do
                        End Do
                 ! Integrating textured polycrystal dielectric tensor components
                 dtexten(i,j) = dtexten(i,j)+R(phi, beta)*drotten(i,j)*dphi*dbeta
                 end do
           end do
   End do
End do

write(*,'(/A)') ' Textured polycrystal tensor:'
Do i = 1,3
         write (*,'(3F10.2)') (dtexten(i,j), j =1,3)
End do

END

subroutine inte2D(omega, suma)
  ! Calculating the surface integral of a Gaussian distribution
  implicit none
  real (kind = 8):: a, b, c, d, pi, omega
  real (kind = 8):: x, y, hx, hy, f, suma
  integer (kind = 4) :: ii, jj, nx, ny

  f(x,y)= exp(-(180.d0*x/(pi*omega))**2) * sin(x)

  pi = 3.141592653589793D+00
  nx = 128*8; ny = 256*8
  a = 0.d0; b = pi; c = 0.d0; d = 2.d0*pi

  hx = (b-a)/dble(nx); hy = (d-c)/dble(ny)
  suma = 0.0D+00
  do ii = 1, nx
     x = a + hx/2.d0 + dble(ii-1)*hx
     do jj = 1, ny
     y = c + hy/2.d0 + dble(jj-1)*hy
     suma = suma + hx * hy * f (x, y)
     end do
  end do
return
end

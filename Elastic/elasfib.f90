Program ELASFIB
    implicit none
    REAL (KIND=8), DIMENSION (6, 6) :: cmat
    REAL (KIND=8), DIMENSION (3,3,3,3) :: cten
    REAL (KIND=8), DIMENSION (3,3) :: Q
    REAL (KIND=8), DIMENSION (3,3,3,3) :: crotten, ctexten
    REAL (KIND=8), DIMENSION (6,6) :: ctexmat
    REAL (KIND=8) :: phi1, phi, phi2, pi, beta, R
    REAL (KIND=8) :: a, b, c, d, dphi, dbeta, omeg, su, total
    INTEGER :: i, j, k, l, m, n, o, p, ll, nn, npas, np, nphi, nb, nbeta, nt
    CHARACTER (LEN=50) :: entra
    INTEGER, PARAMETER :: out_unit = 300

    !dV/V = (1/4*pi)*R(phi,beta)*sin(phi)*dphi*dbeta [Bunge, eq(5.13)]
    R(phi,beta)= (1.d0/su) * exp(-(180.d0*phi/(pi*omeg))**2) * sin(phi)

    nphi = 128*4; nbeta = 256*4

    pi = 3.141592653589793D+00

    a = 0.d0; b = pi; c = 0.d0; d = 2.d0*pi
    dphi= (b-a)/dble(nphi); dbeta=(d-c)/dble(nbeta)

    !call system('cls')
    write(*,'(/A)')' Elasticity for axial textures'
    !write(*,'(/A$)')' Enter data file: '
    !read (*, '(A)') entra
    write(*,'(/A)')' Single crystal matrix in data file:'

    OPEN(unit=1, status='old',file = 'ce.dat', action='read')
    !OPEN(unit=1, status='old',file = entra, action='read')

    Do m=1,6
        read(1,*) (cmat(m,i), i=1,6)
        write (*,'(6F10.2)')(cmat(m,i), i=1,6)
    End Do
    CLOSE(1)

    ! Convert matrix to tensor:
    cten(1,1,1,1) = cmat(1,1);cten(1,1,2,2) = cmat(1,2);cten(1,1,3,3) = cmat(1,3)
    cten(1,1,1,2) = cmat(1,6);cten(1,1,1,3) = cmat(1,5);cten(1,1,2,3) = cmat(1,4)
    cten(1,1,2,1) = cmat(1,6);cten(1,1,3,1) = cmat(1,5);cten(1,1,3,2) = cmat(1,4)
    cten(2,2,1,1) = cmat(2,1);cten(2,2,2,2) = cmat(2,2);cten(2,2,3,3) = cmat(2,3)
    cten(2,2,1,2) = cmat(2,6);cten(2,2,1,3) = cmat(2,5);cten(2,2,2,3) = cmat(2,4)
    cten(2,2,2,1) = cmat(2,6);cten(2,2,3,1) = cmat(2,5);cten(2,2,3,2) = cmat(2,4)
    cten(3,3,1,1) = cmat(3,1);cten(3,3,2,2) = cmat(3,2);cten(3,3,3,3) = cmat(3,3)
    cten(3,3,1,2) = cmat(3,6);cten(3,3,1,3) = cmat(3,5);cten(3,3,2,3) = cmat(3,4)
    cten(3,3,2,1) = cmat(3,6);cten(3,3,3,1) = cmat(3,5);cten(3,3,3,2) = cmat(3,4)
                                                                        
    cten(2,3,1,1) = cmat(4,1);cten(2,3,2,2) = cmat(4,2);cten(2,3,3,3) = cmat(4,3)
    cten(2,3,1,2) = cmat(4,6);cten(2,3,1,3) = cmat(4,5);cten(2,3,2,3) = cmat(4,4)
    cten(2,3,2,1) = cmat(4,6);cten(2,3,3,1) = cmat(4,5);cten(2,3,3,2) = cmat(4,4)
    cten(3,1,1,1) = cmat(5,1);cten(3,1,2,2) = cmat(5,2);cten(3,1,3,3) = cmat(5,3)
    cten(3,1,1,2) = cmat(5,6);cten(3,1,1,3) = cmat(5,5);cten(3,1,2,3) = cmat(5,4)
    cten(3,1,2,1) = cmat(5,6);cten(3,1,3,1) = cmat(5,5);cten(3,1,3,2) = cmat(5,4)
    cten(1,2,1,1) = cmat(6,1);cten(1,2,2,2) = cmat(6,2);cten(1,2,3,3) = cmat(6,3)
    cten(1,2,1,2) = cmat(6,6);cten(1,2,1,3) = cmat(6,5);cten(1,2,2,3) = cmat(6,4)
    cten(1,2,2,1) = cmat(6,6);cten(1,2,3,1) = cmat(6,5);cten(1,2,3,2) = cmat(6,4)
                                                                        
    cten(3,2,1,1) = cmat(4,1);cten(3,2,2,2) = cmat(4,2);cten(3,2,3,3) = cmat(4,3)
    cten(3,2,1,2) = cmat(4,6);cten(3,2,1,3) = cmat(4,5);cten(3,2,2,3) = cmat(4,4)
    cten(3,2,2,1) = cmat(4,6);cten(3,2,3,1) = cmat(4,5);cten(3,2,3,2) = cmat(4,4)
    cten(1,3,1,1) = cmat(5,1);cten(1,3,2,2) = cmat(5,2);cten(1,3,3,3) = cmat(5,3)
    cten(1,3,1,2) = cmat(5,6);cten(1,3,1,3) = cmat(5,5);cten(1,3,2,3) = cmat(5,4)
    cten(1,3,2,1) = cmat(5,6);cten(1,3,3,1) = cmat(5,5);cten(1,3,3,2) = cmat(5,4)
    cten(2,1,1,1) = cmat(6,1);cten(2,1,2,2) = cmat(6,2);cten(2,1,3,3) = cmat(6,3)
    cten(2,1,1,2) = cmat(6,6);cten(2,1,1,3) = cmat(6,5);cten(2,1,2,3) = cmat(6,4)
    cten(2,1,2,1) = cmat(6,6);cten(2,1,3,1) = cmat(6,5);cten(2,1,3,2) = cmat(6,4)


    write(*,'(/A$)')' Enter Omega (degrees): '
    read(*,*) omeg
    call inte2D(omeg, su)

    phi1 = 0.d0   !Fibre texture, Bunge convention

    ! START THE GREAT LOOP FOR POLYCRYSTAL TENSOR COMPONENTS
    Do i = 1, 3
        Do j = 1, 3
            Do k = 1,3
                Do l = 1,3
                    ctexten(i,j,k,l) =0.d0  ! Setting to "0" the textured polycrystal elast tensor component:
                    Do np = 1, nphi  ! loop on polar angle
                        phi = a + dphi/2.d0 + dble(np - 1)*dphi
                        Do nb = 1, nbeta   ! loop on azimuth
                            beta = c + dbeta/2.d0 + dble(nb - 1)*dbeta
                            phi2 = (pi/2.d0) - beta    ! Bunge convention
                            Q(1,1) =  cos(phi2)* cos(phi1) - cos(phi)*sin(phi1)*sin(phi2)
                            Q(1,2) =  cos(phi2)* sin(phi1) + cos(phi)*cos(phi1)*sin(phi2)
                            Q(1,3) =  sin(phi2)* sin(phi)
                            Q(2,1) = -sin(phi2)* cos(phi1) - cos(phi)*sin(phi1)*cos(phi2)
                            Q(2,2) = -sin(phi2)* sin(phi1) + cos(phi)*cos(phi1)*cos(phi2)
                            Q(2,3) =  cos(phi2)* sin(phi)
                            Q(3,1) =  sin(phi) * sin(phi1)
                            Q(3,2) = -sin(phi) * cos(phi1)
                            Q(3,3) =  cos(phi)
                            ! Calculating a rotated crystal elastic tensor component:
                            Do ll = 1, 3
                                Do m = 1, 3
                                    Do n = 1, 3
                                        Do o = 1, 3
                                            crotten(ll,m,n,o) = 0.d0
                                        End Do
                                    End Do
                                End Do
                            End Do
                            Do ll = 1, 3
                                Do m = 1, 3
                                    Do n = 1, 3
                                        Do o = 1, 3
                                        crotten(i,j,k,l) = crotten(i,j,k,l) + Q(i,ll)*Q(j,m)*Q(k,n)*Q(l,o)*cten(ll,m,n,o)
                                        End Do
                                    End Do
                                End Do
                            End Do
                            !Integrating textured polycrystal piezoelectric tensor components
                            ctexten(i,j,k,l) = ctexten(i,j,k,l)+R(phi, beta)*crotten(i,j,k,l)*dphi*dbeta
                        End Do
                    End Do
                End Do
            End Do
        End Do
    End Do

    ! Convert polycrystal tensor to matrix:
    ctexmat(1,1) = ctexten(1,1,1,1);ctexmat(1,2) = ctexten(1,1,2,2);ctexmat(1,3) = ctexten(1,1,3,3)
    ctexmat(1,4) = ctexten(1,1,2,3);ctexmat(1,5) = ctexten(1,1,1,3);ctexmat(1,6) = ctexten(1,1,1,2)
    ctexmat(2,1) = ctexten(2,2,1,1);ctexmat(2,2) = ctexten(2,2,2,2);ctexmat(2,3) = ctexten(2,2,3,3)
    ctexmat(2,4) = ctexten(2,2,2,3);ctexmat(2,5) = ctexten(2,2,1,3);ctexmat(2,6) = ctexten(2,2,1,2)
    ctexmat(3,1) = ctexten(3,3,1,1);ctexmat(3,2) = ctexten(3,3,2,2);ctexmat(3,3) = ctexten(3,3,3,3)
    ctexmat(3,4) = ctexten(3,3,2,3);ctexmat(3,5) = ctexten(3,3,1,3);ctexmat(3,6) = ctexten(3,3,1,2)
    ctexmat(4,1) = ctexten(2,3,1,1);ctexmat(4,2) = ctexten(2,3,2,2);ctexmat(4,3) = ctexten(2,3,3,3)
    ctexmat(4,4) = ctexten(2,3,2,3);ctexmat(4,5) = ctexten(2,3,1,3);ctexmat(4,6) = ctexten(2,3,1,2)
    ctexmat(5,1) = ctexten(3,1,1,1);ctexmat(5,2) = ctexten(3,1,2,2);ctexmat(5,3) = ctexten(3,1,3,3)
    ctexmat(5,4) = ctexten(3,1,2,3);ctexmat(5,5) = ctexten(3,1,1,3);ctexmat(5,6) = ctexten(3,1,1,2)
    ctexmat(6,1) = ctexten(1,2,1,1);ctexmat(6,2) = ctexten(1,2,2,2);ctexmat(6,3) = ctexten(1,2,3,3)
    ctexmat(6,4) = ctexten(1,2,2,3);ctexmat(6,5) = ctexten(1,2,1,3);ctexmat(6,6) = ctexten(1,2,1,2)

    write(*,'(/A)') ' Textured polycrystal matrix:'
    
    Do i = 1,6
        write (*,'(6F10.2)') (ctexmat(i,j), j = 1,6)
    End Do
    
    open (unit=out_unit,file="ce_result.dat",action="write",status="replace")
     Do i = 1,6
        write (out_unit,'(6F10.2)') (ctexmat(i,j), j = 1,6)
    End Do
    close (out_unit)
    
End Program ELASFIB
    
    
Subroutine inte2D(omega, suma)
    ! Calculating the surface integral of a Gaussian distribution
    implicit none
    real (kind = 8):: a, b, c, d, pi, omega
    real (kind = 8):: x, y, hx, hy, f, suma
    integer (kind = 4) :: ii, jj, nx, ny

    f(x,y) = exp(-(180.d0*x/(pi*omega))**2) * sin(x)

    pi = 3.141592653589793D+00
    nx = 1024; ny = 2048
    a = 0.d0; b = pi; c = 0.d0; d = 2.d0*pi

    hx = (b-a)/dble(nx); hy = (d-c)/dble(ny)
    suma = 0.0D+00
    Do ii = 1, nx
        x = a + hx/2.d0 + dble(ii-1)*hx
        Do jj = 1, ny
            y = c + hy/2.d0 + dble(jj-1)*hy
            suma = suma + hx * hy * f (x, y)
        End Do
    End Do
    return
End Subroutine inte2D

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

#compiling and running script for calculations in FORTRAN and drawing in Python
#this script is dual compatible; either on MinGW or proper POCIX system
#hence the ".exe" + "./" abiguity

echo ""
echo " Removing remains of previous runs"
rm BaTiO3_result.dat
rm *.o
rm *.so
rm *.pyc

echo " Compiling FORTRAN calculations code"
f2py -c -m calc_mod  piezofib.f90

echo " "
echo " Running Python script"
python f2py_piezofib.py
echo "Done"
